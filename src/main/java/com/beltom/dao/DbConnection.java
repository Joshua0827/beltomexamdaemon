package com.beltom.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DbConnection {
	private static final Logger log = LogManager.getLogger(DbConnection.class);
	
	private static final String DRIVER_CLASS;
	private static final String USER;
	private static final String PASSWORD;
	private static final String URL;
	
	static  {
		String configFile = System.getProperty("user.dir") + "/connect.properties";
		Properties properties = new Properties();
		
		try {
			properties.load(new FileInputStream(configFile));
		} catch (FileNotFoundException e) {
			log.error("Exception : ", e);
		} catch (IOException e) {
			log.error("Exception : ", e);
		}
		
		DRIVER_CLASS = properties.getProperty("driverClass");
		USER = properties.getProperty("user");
		PASSWORD = properties.getProperty("password");
		URL = properties.getProperty("url");
		
		try {
			Class.forName(DRIVER_CLASS);
		} catch(ClassNotFoundException e) {
			log.error("Exception : ", e);
		}
	}
	
	public static Connection getConnection() {
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch(SQLException e) {
			log.error("Exception : ", e);
		}
		
		return conn;
	}
	
	public static void closeConnection(ResultSet rs, Statement st, Connection conn) {
		if(rs != null) {
			try {
				rs.close();
			} catch(SQLException e) {
				log.error("Exception : ", e);
			}
		}
		
		if(st != null) {
			try {
				st.close();
			} catch(SQLException e) {
				log.error("Exception : ", e);
			}
		}
		
		if(conn != null) {
			try {
				conn.close();
			} catch(SQLException e) {
				log.error("Exception : ", e);
			}
		}
	}
}
