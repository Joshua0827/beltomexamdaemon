package com.beltom.main;

import java.util.Timer;
import java.util.TimerTask;

import com.beltom.task.ShipmentFinish;
import com.beltom.task.ShipmentStart;

public class ShipmentDaemon {
	public static void main(String[] args) throws Exception {
		Timer timer = new Timer();
		TimerTask start = new ShipmentStart();
		TimerTask finish = new ShipmentFinish();
		
		timer.schedule(start, 1000, 30 * 1000);
		timer.schedule(finish, 1000, 300 * 1000);
	}
}
