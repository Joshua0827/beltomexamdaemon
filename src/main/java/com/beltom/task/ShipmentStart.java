package com.beltom.task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.beltom.dao.DbConnection;

public class ShipmentStart extends TimerTask {
	private static final Logger log = LogManager.getLogger(ShipmentStart.class);

	@Override
	public void run() {
		List<String> ids = getIdOfAbleToShip();
		
		for(String id : ids) {
			if(updateShipmentStatus(id))
				insertShipmentHistory(id);
		}
	}
	
	private List<String> getIdOfAbleToShip() {
		List<String> ids = new ArrayList<>();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		String sql = "SELECT shipment_id FROM tb_shipment_m WHERE status = '2'";
		
		try {
			conn = DbConnection.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()) {
				ids.add(rs.getString("shipment_id"));
			}
		} catch(SQLException e) {
			log.error("Exception : ", e);
		} finally {
			DbConnection.closeConnection(rs, st, conn);
		}
		
		return ids;
	}
	
	private boolean updateShipmentStatus(String id) {
		boolean result = false;
		Connection conn = null;
		Statement st = null;
		String sql = "UPDATE tb_shipment_m "
				+ "SET status='3', start_shipment_time=NOW() "
				+ "WHERE shipment_id='" + id + "'";
		
		try {
			conn = DbConnection.getConnection();
			st = conn.createStatement();
			result = (st.executeUpdate(sql) > 0);
		} catch(SQLException e) {
			log.error("Exception : ", e);
		} finally {
			DbConnection.closeConnection(null, st, conn);
		}
		
		return result;
	}
	
	private void insertShipmentHistory(String id) {
		Connection conn = null;
		Statement st = null;
		String sql = "INSERT INTO tb_shipment_history(shipment_id, status, create_date) "
				+ "VALUES('" + id + "', '3', NOW())";
		
		try {
			conn = DbConnection.getConnection();
			st = conn.createStatement();
			st.executeUpdate(sql);
		} catch(SQLException e) {
			log.error("Exception : ", e);
		} finally {
			DbConnection.closeConnection(null, st, conn);
		}
	}
}
